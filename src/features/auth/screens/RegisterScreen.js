import axios from 'axios';
import React, {Component} from 'react';
import {Alert, Text, View} from 'react-native';
import {Button, HelperText, TextInput} from 'react-native-paper';
import {style} from '../styles/RegisterStyle';

class RegisterScreen extends Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordConf: '',
      secureText: true,
      btnDisable: false,
      hasError: false,
      errorMessage: '',
    };
  }

  invalidEmail() {
    const email = this.state.email;
    return email != '' && !email.includes('@');
  }

  passwordMatch() {
    return !(this.state.password === this.state.passwordConf);
  }

  hasErrors() {
    const stateValues = this.state;
    delete stateValues.errorMessage;
    const formValues = Object.values(stateValues);
    // console.log(formValues);
    return formValues.indexOf('') != -1;
  }

  doRegister = async () => {
    try {
      const {firstName, lastName, email, password} = this.state;

      const data = {
        email: email,
        password: password,
      };

      const response = await axios.post('https://reqres.in/api/register', data);
      if (response) this.props.navigation.navigate('Login');
    } catch (error) {
      this.setState({
        hasError: true,
        errorMessage: error.message,
      });
    }
  };

  render() {
    return (
      <View style={style.container}>
        <TextInput
          label="First Name"
          mode="outlined"
          style={[style.inputStyle, style.inpFirstname]}
          value={this.state.firstName}
          onChangeText={(value) => this.setState({firstName: value})}
        />

        <TextInput
          label="Last Name"
          mode="outlined"
          style={[style.inputStyle, style.inpLastname]}
          value={this.state.lastName}
          onChangeText={(value) => this.setState({lastName: value})}
        />

        <TextInput
          label="Email"
          mode="outlined"
          style={[style.inputStyle, style.inpUsername]}
          value={this.state.email}
          onChangeText={(value) => this.setState({email: value})}
        />

        {this.invalidEmail() ? (
          <HelperText
            type="error"
            visible={this.invalidEmail()}
            style={style.hlpPassword}>
            Invalid Email address
          </HelperText>
        ) : null}

        <TextInput
          label="Password"
          mode="outlined"
          style={[style.inputStyle, style.inpPassword]}
          secureTextEntry={this.state.secureText}
          right={
            <TextInput.Icon
              name={this.state.secureText ? 'eye-off' : 'eye'}
              onPress={() =>
                this.setState({secureText: !this.state.secureText})
              }
            />
          }
          value={this.state.password}
          onChangeText={(value) => this.setState({password: value})}
        />

        <TextInput
          label="Confirm Password"
          mode="outlined"
          style={[style.inputStyle, style.inpPassConf]}
          secureTextEntry={this.state.secureText}
          right={
            <TextInput.Icon
              name={this.state.secureText ? 'eye-off' : 'eye'}
              onPress={() =>
                this.setState({secureText: !this.state.secureText})
              }
            />
          }
          value={this.state.passwordConf}
          onChangeText={(value) => this.setState({passwordConf: value})}
        />

        {this.passwordMatch() && (
          <HelperText
            type="error"
            visible={this.passwordMatch()}
            style={style.hlpPassword}>
            Password didn't match!
          </HelperText>
        )}

        {this.state.hasError && (
          <HelperText type="error">{this.state.errorMessage}</HelperText>
        )}

        <Button
          mode="contained"
          style={style.btnRegister}
          contentStyle={style.btnRegisterContent}
          disabled={this.hasErrors()}
          onPress={() => this.doRegister()}>
          Register
        </Button>
      </View>
    );
  }
}

export default RegisterScreen;
