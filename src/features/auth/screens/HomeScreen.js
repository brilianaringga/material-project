import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {Component} from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {Appbar, Avatar, Button, useTheme} from 'react-native-paper';
import {style} from '../styles/HomeStyle';

const Header = ({scene, previous, navigation}) => {
  const theme = useTheme();

  return (
    <Appbar.Header theme={{...theme, colors: {text: 'white'}}}>
      {previous ? (
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      ) : null}

      <Appbar.Action icon="menu" />
      <Appbar.Content
        title={scene.route?.name}
        titleStyle={{fontSize: 24, fontWeight: 'bold'}}
      />
      <Appbar.Action icon="bell" />
      <Appbar.Action
        icon="exit-to-app"
        onPress={() => {
          navigation.reset({
            index: 0,
            routes: [{name: 'Login'}],
          });
        }}
      />
    </Appbar.Header>
  );
};

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      token: null,
    };
  }

  componentDidMount() {
    const {username} = this.props.route.params;

    this.props.navigation.setOptions({
      header: (props) => {
        return <Header {...props} />;
      },
    });

    this.getToken().then(() => this.getUsers());
  }

  componentDidUpdate() {}

  getToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      console.log(token);
      if (token) {
        this.setState({
          token: token,
        });
      }
    } catch (error) {
      alert('Gagal mendapatkan token');
    }
  };

  getUsers = async () => {
    try {
      const response = await axios.get('https://reqres.in/api/users');
      const users = response.data.data;
      this.setState({
        users: users,
      });
    } catch (error) {
      console.log(error.message);
    }
  };

  logoutHandler() {
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  }

  render() {
    return (
      <View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={style.container}>
            {this.state.users.map((user, index) => {
              return (
                <View key={index} style={style.rootContainer}>
                  <View style={style.userContainer}>
                    <Avatar.Image
                      source={{uri: user.avatar}}
                      size={150}
                      style={style.avatar}
                    />
                    <View style={style.detail}>
                      <Text style={style.username}>
                        {user.first_name} {user.last_name}
                      </Text>
                      <Text style={style.email}>{user.email}</Text>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default HomeScreen;
