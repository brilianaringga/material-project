import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {Component} from 'react';
import {
  Alert,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {Button, HelperText, TextInput} from 'react-native-paper';
import {style} from '../styles/LoginStyle';

class LoginScreen extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      secureText: true,
      errors: false,
      token: null,
    };
  }

  componentDidMount() {
    // this.props.navigation.setOptions({headerShown: false});
  }

  formIsValid() {
    const formValue = Object.values(this.state);
    return formValue.indexOf('') == -1;
  }

  registerHandler() {
    this.props.navigation.navigate('Register');
  }

  showPassword() {
    this.setState({
      secureText: !this.state.secureText,
    });
  }

  storeToken = async () => {
    try {
      await AsyncStorage.setItem('token', this.state.token);

      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'Home', params: {username: this.state.username}}],
      });
    } catch (error) {
      alert('Gagal menyimpan data!');
    }
  };

  doLogin = async () => {
    if (!this.formIsValid()) {
      Alert.alert('Username and Password cannot be empty');
      return;
    }

    // eve.holt@reqres.in

    let data = {
      email: this.state.username,
      password: this.state.password,
    };

    try {
      const response = await axios.post('https://reqres.in/api/login', data);
      // console.log(response.data.token);

      this.setState(
        {
          token: response.data.token,
        },
        () => {
          this.storeToken();
        },
      );
    } catch (error) {
      this.setState({
        errors: true,
        errorMessage: error.message,
      });
    }
  };

  hasErrors() {
    return this.state.errors;
  }

  render() {
    return (
      <SafeAreaView style={style.safeArea}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={style.keyboardWrapper}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={style.container}>
              <View style={style.logoContainer}>
                <View style={style.containerLingkaran}>
                  <View style={style.lingkaran} />
                </View>
                <Image source={require('../../../assets/logo.png')} />
              </View>
              <View style={style.formContainer}>
                <TextInput
                  label="Username"
                  mode="outlined"
                  style={style.inputUsername}
                  value={this.state.username}
                  onChangeText={(value) => this.setState({username: value})}
                />
                <TextInput
                  label="Password"
                  mode="outlined"
                  secureTextEntry={this.state.secureText}
                  style={style.inputPassword}
                  value={this.state.password}
                  onChangeText={(value) => this.setState({password: value})}
                  right={
                    <TextInput.Icon
                      name={this.state.secureText ? 'eye-off' : 'eye'}
                      color={'#000000'}
                      onPress={() => this.showPassword()}
                    />
                  }
                />

                {this.hasErrors() && (
                  <HelperText type="error" visible={this.hasErrors()}>
                    {this.state.errorMessage}
                  </HelperText>
                )}

                <Button
                  mode="contained"
                  style={style.btnSubmit}
                  contentStyle={style.btnSubmitContent}
                  onPress={() => this.doLogin()}>
                  Login
                </Button>
                <Button
                  style={style.btnRegister}
                  contentStyle={style.btnRegisterContent}
                  onPress={() => this.registerHandler()}>
                  Register
                </Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

export default LoginScreen;
