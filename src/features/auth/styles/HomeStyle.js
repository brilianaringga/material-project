import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    padding: 15,
    paddingBottom: 50,
  },
  textWelcome: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 50,
  },
  rootContainer: {
    paddingTop: 30,
    paddingLeft: 70,
  },
  userContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderTopRightRadius: 25,
    borderBottomEndRadius: 5,
    backgroundColor: '#008080',
  },
  avatar: {
    marginLeft: -70,
    marginRight: 10,
    marginTop: -30,
    borderWidth: 2,
    borderColor: '#008080',
  },
  detail: {
    alignItems: 'flex-end',
    flexDirection: 'column',
    // justifyContent: 'center',
    flex: 1,
    paddingTop: 20,
    paddingEnd: 15,
  },
  username: {
    fontSize: 24,
    fontWeight: '800',
    // fontStyle: 'italic',
    color: 'white',
  },
  email: {
    fontSize: 16,
    color: 'white',
  },
});
