import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    paddingTop: 15,
    paddingHorizontal: 15,
  },
  inputStyle: {
    marginBottom: 5,
  },
  btnRegister: {
    marginTop: 10,
  },
  btnRegisterContent: {
    paddingVertical: 5,
  },
});
