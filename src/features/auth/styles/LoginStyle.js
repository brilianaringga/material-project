import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  keyboardWrapper: {
    flex: 1,
  },
  // touchableWrapper: {
  // flexGrow: 1,
  // flex: 1,
  // backgroundColor: 'yellow',
  // flexDirection: 'column'
  // justifyContent: 'flex-end',
  // },
  container: {
    height: '100%',
    justifyContent: 'flex-end',
    // backgroundColor: 'red',
  },
  containerLingkaran: {
    // backgroundColor: 'red',
    position: 'absolute',
    // top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lingkaran: {
    bottom: 10,
    height: 1000,
    width: 1000,
    backgroundColor: '#008080',
    borderRadius: 500,
  },
  logoContainer: {
    // backgroundColor: 'green',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    paddingHorizontal: 25,
    marginBottom: 25,
  },
  inputUsername: {
    marginBottom: 10,
  },
  inputPassword: {
    marginBottom: 10,
  },
  btnSubmit: {
    marginVertical: 5,
  },
  btnSubmitContent: {
    paddingVertical: 5,
  },
  btnRegister: {
    marginVertical: 5,
  },
  btnRegisterContent: {
    paddingVertical: 5,
  },
});
