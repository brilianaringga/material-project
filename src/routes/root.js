import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Appbar, useTheme} from 'react-native-paper';
import HomeScreen from '../features/auth/screens/HomeScreen';
import LoginScreen from '../features/auth/screens/LoginScreen';
import RegisterScreen from '../features/auth/screens/RegisterScreen';

const Stack = createStackNavigator();

const Header = ({scene, previous, navigation}) => {
  return (
    <Appbar.Header>
      {previous ? (
        <Appbar.BackAction onPress={() => navigation.goBack()} />
      ) : null}

      <Appbar.Content
        title={scene.route?.name}
        titleStyle={{fontSize: 24, fontWeight: 'bold'}}
      />
    </Appbar.Header>
  );
};

const root = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        header: (props) => <Header {...props} />,
      }}>
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Register" component={RegisterScreen} />
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
  );
};

export default root;
